﻿namespace BashNIPIOilTestProject
{
    partial class f_mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tv_objects = new System.Windows.Forms.TreeView();
            this.dv_data = new System.Windows.Forms.DataGridView();
            this.cm_nodeTreeProject = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.добавитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.свойстваToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cm_nodeTreeObject = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.добавитьОбъектToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьКомплектToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.свойстваToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cm_nodeTreeSet = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.добавитьДокументToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.свойстваToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dv_data)).BeginInit();
            this.cm_nodeTreeProject.SuspendLayout();
            this.cm_nodeTreeObject.SuspendLayout();
            this.cm_nodeTreeSet.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tv_objects);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dv_data);
            this.splitContainer1.Size = new System.Drawing.Size(804, 481);
            this.splitContainer1.SplitterDistance = 268;
            this.splitContainer1.TabIndex = 0;
            // 
            // tv_objects
            // 
            this.tv_objects.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tv_objects.Location = new System.Drawing.Point(0, 0);
            this.tv_objects.Name = "tv_objects";
            this.tv_objects.Size = new System.Drawing.Size(268, 481);
            this.tv_objects.TabIndex = 0;
            this.tv_objects.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            this.tv_objects.Click += new System.EventHandler(this.tv_objects_Click);
            // 
            // dv_data
            // 
            this.dv_data.AllowUserToAddRows = false;
            this.dv_data.AllowUserToDeleteRows = false;
            this.dv_data.AllowUserToOrderColumns = true;
            this.dv_data.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dv_data.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dv_data.Location = new System.Drawing.Point(0, 0);
            this.dv_data.Name = "dv_data";
            this.dv_data.ReadOnly = true;
            this.dv_data.Size = new System.Drawing.Size(532, 481);
            this.dv_data.TabIndex = 0;
            // 
            // cm_nodeTreeProject
            // 
            this.cm_nodeTreeProject.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьToolStripMenuItem,
            this.свойстваToolStripMenuItem,
            this.удалитьToolStripMenuItem});
            this.cm_nodeTreeProject.Name = "cm_nodeTreeProject";
            this.cm_nodeTreeProject.Size = new System.Drawing.Size(139, 70);
            this.cm_nodeTreeProject.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // добавитьToolStripMenuItem
            // 
            this.добавитьToolStripMenuItem.Name = "добавитьToolStripMenuItem";
            this.добавитьToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.добавитьToolStripMenuItem.Text = "Добавить ...";
            // 
            // свойстваToolStripMenuItem
            // 
            this.свойстваToolStripMenuItem.Name = "свойстваToolStripMenuItem";
            this.свойстваToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.свойстваToolStripMenuItem.Text = "Свойства ...";
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            // 
            // cm_nodeTreeObject
            // 
            this.cm_nodeTreeObject.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьОбъектToolStripMenuItem,
            this.добавитьКомплектToolStripMenuItem,
            this.свойстваToolStripMenuItem1,
            this.удалитьToolStripMenuItem1});
            this.cm_nodeTreeObject.Name = "cm_nodeTreeObject";
            this.cm_nodeTreeObject.Size = new System.Drawing.Size(195, 92);
            // 
            // добавитьОбъектToolStripMenuItem
            // 
            this.добавитьОбъектToolStripMenuItem.Name = "добавитьОбъектToolStripMenuItem";
            this.добавитьОбъектToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.добавитьОбъектToolStripMenuItem.Text = "Добавить объект ...";
            // 
            // добавитьКомплектToolStripMenuItem
            // 
            this.добавитьКомплектToolStripMenuItem.Name = "добавитьКомплектToolStripMenuItem";
            this.добавитьКомплектToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.добавитьКомплектToolStripMenuItem.Text = "Добавить комплект ...";
            // 
            // свойстваToolStripMenuItem1
            // 
            this.свойстваToolStripMenuItem1.Name = "свойстваToolStripMenuItem1";
            this.свойстваToolStripMenuItem1.Size = new System.Drawing.Size(194, 22);
            this.свойстваToolStripMenuItem1.Text = "Свойства ...";
            // 
            // удалитьToolStripMenuItem1
            // 
            this.удалитьToolStripMenuItem1.Name = "удалитьToolStripMenuItem1";
            this.удалитьToolStripMenuItem1.Size = new System.Drawing.Size(194, 22);
            this.удалитьToolStripMenuItem1.Text = "Удалить";
            // 
            // cm_nodeTreeSet
            // 
            this.cm_nodeTreeSet.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.добавитьДокументToolStripMenuItem,
            this.свойстваToolStripMenuItem2,
            this.удалитьToolStripMenuItem2});
            this.cm_nodeTreeSet.Name = "cm_nodeTreeSet";
            this.cm_nodeTreeSet.Size = new System.Drawing.Size(194, 70);
            // 
            // добавитьДокументToolStripMenuItem
            // 
            this.добавитьДокументToolStripMenuItem.Name = "добавитьДокументToolStripMenuItem";
            this.добавитьДокументToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.добавитьДокументToolStripMenuItem.Text = "Добавить документ ...";
            // 
            // свойстваToolStripMenuItem2
            // 
            this.свойстваToolStripMenuItem2.Name = "свойстваToolStripMenuItem2";
            this.свойстваToolStripMenuItem2.Size = new System.Drawing.Size(193, 22);
            this.свойстваToolStripMenuItem2.Text = "Свойства ...";
            // 
            // удалитьToolStripMenuItem2
            // 
            this.удалитьToolStripMenuItem2.Name = "удалитьToolStripMenuItem2";
            this.удалитьToolStripMenuItem2.Size = new System.Drawing.Size(193, 22);
            this.удалитьToolStripMenuItem2.Text = "Удалить";
            // 
            // f_mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 481);
            this.Controls.Add(this.splitContainer1);
            this.Name = "f_mainForm";
            this.Text = "BashNIPIOil Test Project";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dv_data)).EndInit();
            this.cm_nodeTreeProject.ResumeLayout(false);
            this.cm_nodeTreeObject.ResumeLayout(false);
            this.cm_nodeTreeSet.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView tv_objects;
        private System.Windows.Forms.ContextMenuStrip cm_nodeTreeProject;
        private System.Windows.Forms.ContextMenuStrip cm_nodeTreeObject;
        private System.Windows.Forms.ContextMenuStrip cm_nodeTreeSet;
        private System.Windows.Forms.DataGridView dv_data;
        private System.Windows.Forms.ToolStripMenuItem добавитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem свойстваToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem добавитьОбъектToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem добавитьКомплектToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem свойстваToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem добавитьДокументToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem свойстваToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem2;
    }
}

